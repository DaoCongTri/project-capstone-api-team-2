var showMessage = (id, message) => {
  document.getElementById(id).innerHTML = message;
};
var checkNull = (idErr, value) => {
  if (value.length == 0 || value == 0) {
    showMessage(idErr, "This entry cannot be left blank");
    return false;
  } else {
    showMessage(idErr, "");
    return true;
  }
};
var checkPrice = (price) => {
  if (/^[0-9]/.test(price)) {
    showMessage("spanPrice", "");
    return true;
  } else {
    showMessage("spanPrice", "Please enter the price in numbers!!!");
    return false;
  }
};
var checkType = (type) => {
  if (type == "Samsung" || type == "iPhone") {
    showMessage("spanType", "");
    return true;
  } else {
    showMessage(
      "spanType",
      "Please enter one of two categories: Samsung and iPhone!!!"
    );
    return false;
  }
};
