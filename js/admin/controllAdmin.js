function renderAdmin(productArr) {
  var contentHTML = "";
  for (var i = 0; i < productArr.length; i++) {
    var product = productArr[i];
    var contentTr = ` <tr>
            <td>${product.id}</td>
            <td>${product.name}</td>
            <td>
                <img src="${
                  product.img
                }" class="img-thumbnail" width="100%" height="100%">
            </td>
            <td>${product.screen}</td>
            <td>${product.bCamera}</td>
            <td>${product.fCamera}</td>
            <td>${product.desc}</td>
            <td>${product.type}</td>
            <td>${new Intl.NumberFormat().format(product.price)} VND</td>
            <td class="flex mt-5">
               <button onclick="deleteProductAdmin(${
                 product.id
               })" class="btn btn-danger mr-2" >Delete</button>
               <button 
               onclick="fixProductAdmin(${product.id})"
               class="btn btn-secondary" >Fix</button>
            </td>
    </tr> `;
    contentHTML = contentHTML + contentTr;
  }
  document.getElementById("tbodyProduct").innerHTML = contentHTML;
}
