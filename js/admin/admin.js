const BASE_URL = "https://643a58babd3623f1b9b1645c.mockapi.io/productAPI";
var idSelected = 0;
var productArr = [];
var dataJson = localStorage.getItem("LIST_OF_PRODUCT_LOCAL");
if (dataJson != null) {
  var dataArr = JSON.parse(dataJson);
  for (var i = 0; i < dataArr.length; i++) {
    var item = dataArr[i];
    var product = new Product(
      item.id,
      item.name,
      item.price,
      item.screen,
      item.bCamera,
      item.fCamera,
      item.img,
      item.desc,
      item.type
    );
    productArr.push(product);
    console.log("productArr", productArr);
  }
}
function fetchProDuct() {
  batLoading();
  productService
    .getList()
    .then((res) => {
      tatLoading();
      console.log(res);
      renderAdmin(res.data.reverse());
      var dataJson = JSON.stringify(res.data);
      localStorage.setItem("LIST_OF_PRODUCT_LOCAL", dataJson);
    })
    .catch((err) => {
      tatLoading();
      console.log(err);
    });
}
fetchProDuct();
// add product admin
function addProductAdmin() {
  var product = layThongTinTuForm();
  var isValid = checkNull("spanName", product.name);
  isValid =
    isValid & checkNull("spanPrice", product.price) &&
    checkPrice(product.price);
  isValid = isValid & checkNull("spanScreen", product.screen);
  isValid = isValid & checkNull("spanBCamera", product.bCamera);
  isValid = isValid & checkNull("spanFCamera", product.fCamera);
  isValid = isValid & checkNull("spanImg", product.img);
  isValid =
    isValid & checkNull("spanType", product.type) && checkType(product.type);
  isValid = isValid & checkNull("spanDesc", product.desc);
  if (!isValid) {
    return;
  }
  productService
    .create(product)
    .then((res) => {
      fetchProDuct();
      reset();
      console.log(res);
      Toastify({
        text: "Successfully added new products!!!",
        offset: {
          x: 50,
          y: 10,
        },
      }).showToast();
    })
    .catch((err) => {
      console.log(err);
    });
}
// delete product admin
function deleteProductAdmin(id) {
  batLoading();
  productService
    .remove(id)
    .then((res) => {
      fetchProDuct();
      console.log(res);
      Toastify({
        text: "Successfully deleted products!!!",
        offset: {
          x: 50,
          y: 10,
        },
      }).showToast();
    })
    .catch((err) => {
      tatLoading();
      console.log(err);
    });
}
// fix product admin
function fixProductAdmin(id) {
  batLoading();
  idSelected = id;
  productService
    .fix(id)
    .then((res) => {
      tatLoading();
      showThongTinLenForm(res.data);
      console.log(res);
    })
    .catch((err) => {
      tatLoading();
      console.log(err);
    });
}
// update product admin
function updateProductAdmin() {
  batLoading();
  var product = layThongTinTuForm();
  var isValid = checkNull("spanName", product.name);
  isValid =
    isValid & checkNull("spanPrice", product.price) &&
    checkPrice(product.price);
  isValid = isValid & checkNull("spanScreen", product.screen);
  isValid = isValid & checkNull("spanBCamera", product.bCamera);
  isValid = isValid & checkNull("spanFCamera", product.fCamera);
  isValid = isValid & checkNull("spanImg", product.img);
  isValid =
    isValid & checkNull("spanType", product.type) && checkType(product.type);
  isValid = isValid & checkNull("spanDesc", product.desc);
  if (!isValid) {
    return;
  }
  axios({
    url: `${BASE_URL}/${idSelected}`,
    method: "PUT",
    data: product,
  })
    .then((res) => {
      tatLoading();
      fetchProDuct();
      Toastify({
        text: "Successfully updated products",
        offset: {
          x: 50,
          y: 10,
        },
      }).showToast();
      console.log(res);
    })
    .catch((err) => {
      tatLoading();
      console.log(err);
    });
}
// search product admin
function searchProductAdmin() {
  batLoading();
  var request = document.getElementById("txtSearch").value.trim();
  console.log(request);
  axios({
    url: BASE_URL,
    method: "GET",
    params: {
      name: request,
    },
  })
    .then((res) => {
      tatLoading();
      console.log(res.data);
      var result = res.data;
      renderAdmin(result);
      Toastify({
        text: "Successfully searched products",
        offset: {
          x: 50,
          y: 10,
        },
      }).showToast();
      console.log(res);
    })

    .catch((err) => {
      tatLoading();
      console.log(err);
    });
}
/*
function text(searchTerms) {
  axios({
    // url: https://643a58bdbd3623f1b9b164ba.mockapi.io/admin${id ? `/${id} : ""}`,
    url: BASE_URL, //https://643a58bdbd3623f1b9b164ba.mockapi.io/admin,
    method: "GET",
    params: {
      name: searchTerms,
    },
  })
    .then((res) => {
      console.log("res: ", res);
      console.log("res: ", res.data);
      // if (!id) render(res.data);
      // resolve(res);
    })
    .catch((err) => {
      console.log("err: ", err);
    });
}
text("iPhone");
*/
function ascending() {
  var arrSort1 = productArr.sort((a, b) => a.price - b.price);
  console.log(arrSort1);
  renderAdmin(arrSort1);
}
function decrease() {
  var arrSort2 = productArr.sort((a, b) => b.price - a.price);
  console.log(arrSort2);
  renderAdmin(arrSort2);
}
function reset() {
  document.querySelector("#formProductAdmin").reset();
}
