// API product user
const BASE_URL = "https://643a58babd3623f1b9b1645c.mockapi.io/productAPI";
var productArr = [];
var idSelected = 0;
var dataJson = localStorage.getItem("LIST_OF_PRODUCT_LOCAL");
if (dataJson != null) {
  var dataArr = JSON.parse(dataJson);
  for (var i = 0; i < dataArr.length; i++) {
    var item = dataArr[i];
    var product = new Product(
      item.id,
      item.name,
      item.price,
      item.screen,
      item.bCamera,
      item.fCamera,
      item.img,
      item.desc,
      item.type
    );
    productArr.push(product);
    console.log("productArr", productArr);
  }
}
// fetch product user
function fetchUser() {
  productService
    .getList()
    .then((res) => {
      console.log(res);
      renderProduct(res.data);
      var dataJson = JSON.stringify(res.data);
      localStorage.setItem("LIST_OF_PRODUCT_LOCAL", dataJson);
    })
    .catch((err) => {
      console.log(err);
    });
}
fetchUser();
// API product cart
const BASE_URL_CART = "https://643a58babd3623f1b9b1645c.mockapi.io/cartItem";
var cart = [];
var dataJsonCart = localStorage.getItem("LIST_OF_PRODUCT_CART");
if (dataJsonCart != null) {
  var dataArrCart = JSON.parse(dataJsonCart);
  for (var i = 0; i < dataArrCart.length; i++) {
    var item = dataArrCart[i];
    var productCart = new CartItem(
      item.price,
      item.name,
      item.img,
      item.quantity
    );
    cart.push(productCart);
    console.log("cart", cart);
  }
}
// xử lý tăng giảm quantity
document.querySelector("#yourCart").addEventListener("click", function (e) {
  const el = e.target;
  const btnDown = el.closest(".down");
  const btnUp = el.closest(".up");
  // 2 cái class down và up này em gắn trong file controllerUser hàm renderProductCart chỗ 2 cái button tăng giảm quantity
  if (btnDown) {
    console.log(btnDown.dataset.id);
    // dataset: em lấy data từ chỗ data-id ngay chỗ 2 nút button tăng giảm quantity
    downCart(btnDown.dataset.id);
  }
  if (btnUp) {
    console.log(btnUp.dataset.id);
    upCart(btnUp.dataset.id);
  }
});
// giảm quantity
function downCart(id) {
  // findIndex(): là hàm tìm phần tử nào mà thỏa điều kiện thì nó chỉ nhận 1 phần tử đó
  // còn map(): là hàm tìm phần tử mà thỏa điều kiện thì nó có thể nhận nhiều phần tử nếu thỏa điều kiện
  const index = cart.findIndex((item) => {
    if (item.id === id) {
      // tìm được id lúc bấm giảm quantity thì nó dừng lưu lại trong index
      return true;
    }
  });
  cart[index].quantity--; // phần tử đối tượng cart thứ index, trường quantiy sẽ giảm đi 1
  console.log(cart[index].quantity);
  if (cart[index].quantity === 0) {
    // nếu quantiy của đối tượng cart thứ index === 0 thì sẽ gọi axios để xóa phần tử đó ra khỏi mảng và api
    cartItemService
      .remove(id)
      .then((res) => {
        fetchProductCart();
        notification("Successfully deleted products from cart!!!");
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
    return;
  }
  // nếu chưa giảm hết để xóa thì sẽ update lại giá trị trong api và mảng để tính toán total
  cartItemService
    .update(id, cart[index])
    .then((res) => {
      fetchProductCart();
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
}
// tăng quantity
function upCart(id) {
  const index = cart.findIndex((item) => {
    if (item.id === id) {
      // tìm được id lúc bấm tăng quantity thì nó dừng lưu lại trong index
      return true;
    }
  });
  cart[index].quantity++; // phần tử đối tượng cart thứ index, trường quantiy sẽ tăng lên 1
  // tăng quantity lên thì sẽ update lại api và mảng để tính toán total
  cartItemService
    .update(id, cart[index])
    .then((res) => {
      fetchProductCart();
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
}
// fetch product cart
function fetchProductCart() {
  cartItemService
    .getList()
    .then((res) => {
      cart = res.data;
      console.log(res);
      renderProductCart(cart);
      var dataArrCart = JSON.stringify(cart);
      localStorage.setItem("LIST_OF_PRODUCT_CART", dataArrCart);
    })
    .catch((err) => {
      console.log(err);
    });
}
fetchProductCart();
// filter select
function filterProducts(type) {
  return productArr.filter((product) => product.type === type);
}
function selectProduct() {
  var selectType = document.getElementById("selectType").value;
  console.log(selectType);
  axios({
    url: BASE_URL,
    method: "GET",
  })
    .then((res) => {
      if (selectType == "type") {
        renderProduct(res.data);
      } else {
        var filterSelect = filterProducts(selectType);
        renderProduct(filterSelect);
      }
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
}
// search product user
function searchProduct() {
  var request = document.getElementById("txtSearch").value.trim();
  document.getElementById("txtSearch").value = "";
  console.log(request);
  axios({
    url: BASE_URL,
    method: "GET",
    params: {
      name: request,
    },
  })
    .then((res) => {
      console.log(res.data);
      renderProduct(res.data);
      Toastify({
        text: "Successfully searched products",
        offset: {
          x: 50,
          y: 10,
        },
      }).showToast();
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
}
//--------------------- cart item ------------------------//
// add to cart
function addToCart(id) {
  console.log(id);
  var product = productArr.find((item) => item.id == id);
  // em tìm trong mảng cart xem lúc add nếu có trùng thì sẽ dừng và ko tạo thêm 1 sản phẩm trùng nữa mà chỉ tăng quantity thôi
  const index = cart.findIndex((item) => {
    if (item.name === product.name) {
      return true;
    }
  });
  if (index !== -1) {
    // tìm thấy
    const id = cart[index].id;
    cart[index].quantity++;
    cartItemService
      .update(id, cart[index])
      .then((res) => {
        fetchProductCart();
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
    return;
  }
  console.log(product.name);
  // em tạo lại new object để nó chỉ lấy những thông tin cần thiết khi add to cart
  var cartItem = new CartItem(product.price, product.name, product.img, 1); // 1 này là tượng trưng cho quantity = 1
  cartItemService
    .create(cartItem)
    .then((res) => {
      fetchProductCart();
      notification("Successfully added new products to cart!!!");
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
}
// remove product from cart
function removeCart(id) {
  cartItemService
    .remove(id)
    .then((res) => {
      fetchProductCart();
      notification("Successfully deleted products from cart!!!");
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
}
// trong forEach ko thể sử dụng async - await chỉ có thể sử dụng trong vòng lặp for thường
// async await là phương thức bắt axios chờ await chạy xong mới chạy tới promise(then, catch)
async function deleteAll() {
  // hàm này dùng để bấm thanh toán sẽ xóa hết sản phẩm trong giỏ hàng
  try {
    // cart.forEach
    for (let index = 0; index < cart.length; index++) {
      const id = cart[index].id;
      await cartItemService.remove(id);
    }
    fetchProductCart();
  } catch (error) {
    console.log(error);
  }
}
