function renderProduct(productArr) {
  var content = "";
  for (var i = 0; i < productArr.length; i++) {
    var product = productArr[i];
    var contentDiv = ` 
    <div class="work-item wow">
      <img src="${product.img}" alt="" />
        <div class="item-text">
          <p>Name: ${product.name}</p>
          <p>Screen: ${product.screen}</p>
          <p>Black Camera: ${product.bCamera}</p>
          <p>Front Camera: ${product.fCamera}</p>
          <p>Desc: ${product.desc}</p>
          <p>Type: ${product.type}</p>
          <p>Price: ${new Intl.NumberFormat().format(product.price)} VND</p>
        </div>
        <div class="overlay">
          <div class="overlay-text">
            <button
              class="btn btn-primary"
              data-name="${product.name}"
              data-price="${product.price}"
              type="submit"
              onclick="addToCart(${product.id})"
            >
              Add To Cart
            </button>
            </div>
          </div>
    </div>
    `;
    content = content + contentDiv;
  }
  document.getElementById("productUser").innerHTML = content;
  return content;
}
function renderProductCart(cart) {
  var contentCart = "";
  var total = 0;
  document.getElementById("count").innerHTML = cart.length;
  if (cart.length == 0) {
    document.getElementById("totalPrice").innerHTML = 0 + " VND";
  } else {
    for (var i = 0; i < cart.length; i++) {
      var productCart = cart[i];
      var contentTr = ` <tr class="h-24" id="cartRow">
              <td>${i}</td>
              <td class="w-56">${productCart.name}</td>
              <td class="w-28">
                <img
                  src="${productCart.img}"
                  alt=""
                  width="120px"
                  height="120px"
                />
              </td>
              <td id="cartPrice">${new Intl.NumberFormat().format(
                productCart.price
              )} VND</td>
              <td class="justify-between">
                <button type="button" class="btn btn-secondary down" data-id="${
                  productCart.id
                }">
                  <i class="w-5 fa-solid fa-minus fa-lg"></i>
                </button>
                <span >${productCart.quantity}</span>
                <button type="button" class="btn btn-secondary up" data-id="${
                  productCart.id
                }">
                  <i class="w-5 fa-solid fa-plus fa-lg"></i>
                </button>
              </td>
              <td>
                <button class="btn btn-primary" onclick="removeCart(${
                  productCart.id
                })" data-name="${productCart.name}">
                  Remove
                </button>
              </td>
            </tr>
    `;
      contentCart = contentCart + contentTr;
      total += productCart.price * productCart.quantity;
      document.getElementById("totalPrice").innerHTML =
        new Intl.NumberFormat().format(total) + " VND";
    }
  }
  document.getElementById("yourCart").innerHTML = contentCart;
  return contentCart;
}
function payment() {
  if (cart.length === 0) {
    alert("Your cart is empty..");
    return;
  }
  deleteAll();
  alert("Thank you for your purchase!");
}
// Get the modal
var modal = document.getElementById("myModal");
// Get the button that opens the modal
var btn = document.getElementById("myBtn");
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];
// When the user clicks the button, open the modal
btn.onclick = function () {
  modal.style.display = "block";
};
// When the user clicks on <span> (x), close the modal
span.onclick = function () {
  modal.style.display = "none";
};
// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
};
// hàm này chỉ show thông báo
function notification(mes) {
  Toastify({
    text: mes,
    offset: {
      x: 50,
      y: 10,
    },
    onclick: function () {},
  }).showToast();
}
