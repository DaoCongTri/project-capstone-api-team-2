class Product {
  constructor(id, name, price, screen, bCamera, fCamera, img, desc, type) {
    this.id = id;
    this.name = name;
    this.price = price;
    this.screen = screen;
    this.bCamera = bCamera;
    this.fCamera = fCamera;
    this.img = img;
    this.desc = desc;
    this.type = type;
  }
}
class CartItem {
  constructor(price, name, img, quantity) {
    this.price = price;
    this.name = name;
    this.img = img;
    this.quantity = quantity;
  }
}
