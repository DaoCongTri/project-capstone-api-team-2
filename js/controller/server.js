var productService = {
  getList: () => {
    return axios({
      url: `${BASE_URL}`,
      method: "GET",
    });
  },
  remove: (id) => {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "DELETE",
    });
  },
  create: (data1) => {
    return axios({
      url: BASE_URL,
      method: "POST",
      data: data1,
    });
  },
  fix: (id) => {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "GET",
    });
  },
};
var cartItemService = {
  getList: () => {
    return axios({
      url: `${BASE_URL_CART}`,
      method: "GET",
    });
  },
  remove: (id) => {
    return axios({
      url: `${BASE_URL_CART}/${id}`,
      method: "DELETE",
    });
  },
  create: (dataItem) => {
    return axios({
      url: BASE_URL_CART,
      method: "POST",
      data: dataItem,
    });
  },
  update: (id, dataItem) => {
    return axios({
      url: `${BASE_URL_CART}/${id}`,
      method: "PUT",
      data: dataItem,
    });
  },
};
