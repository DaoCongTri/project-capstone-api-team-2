// file controller dùng chung cho user và admin
function layThongTinTuForm() {
  var id = document.getElementById("txtID").value;
  var name = document.getElementById("txtName").value;
  var price = document.getElementById("txtPrice").value * 1;
  var screen = document.getElementById("txtScreen").value;
  var bCamera = document.getElementById("txtBCamera").value;
  var fCamera = document.getElementById("txtFCamera").value;
  var img = document.getElementById("txtImg").value;
  var desc = document.getElementById("txtDesc").value;
  var type = document.getElementById("txtType").value;
  var product = new Product(
    id,
    name,
    price,
    screen,
    bCamera,
    fCamera,
    img,
    desc,
    type
  );
  return product;
}
function showThongTinLenForm(product) {
  document.getElementById("txtID").value = product.id;
  document.getElementById("txtName").value = product.name;
  document.getElementById("txtPrice").value = product.price;
  document.getElementById("txtScreen").value = product.screen;
  document.getElementById("txtBCamera").value = product.bCamera;
  document.getElementById("txtFCamera").value = product.fCamera;
  document.getElementById("txtImg").value = product.img;
  document.getElementById("txtDesc").value = product.desc;
  document.getElementById("txtType").value = product.type;
}
function batLoading() {
  document.getElementById("loading").style.display = "flex";
}
function tatLoading() {
  document.getElementById("loading").style.display = "none";
}
// back to top
(function () {
  var backTop = document.getElementsByClassName("js-cd-top")[0],
    offset = 300,
    scrollDuration = 700,
    scrolling = false;
  if (backTop) {
    window.addEventListener("scroll", function (event) {
      if (!scrolling) {
        scrolling = true;
        !window.requestAnimationFrame
          ? setTimeout(checkBackToTop, 250)
          : window.requestAnimationFrame(checkBackToTop);
      }
    });
    backTop.addEventListener("click", function (event) {
      event.preventDefault();
      !window.requestAnimationFrame
        ? window.scrollTo(0, 0)
        : Util.scrollTo(0, scrollDuration);
    });
  }
  function checkBackToTop() {
    var windowTop = window.scrollY || document.documentElement.scrollTop;
    windowTop > offset
      ? Util.addClass(backTop, "cd-top--is-visible")
      : Util.removeClass(backTop, "cd-top--is-visible cd-top--fade-out");
    windowTop > offsetOpacity && Util.addClass(backTop, "cd-top--fade-out");
    scrolling = false;
  }
})();
